# Raspbian


_tbd_




## References

* [Raspbian on QEMU with network access](https://ownyourbits.com/2017/02/06/raspbian-on-qemu-with-network-access/)
* [Emulate a Raspberry Pi with Qemu + KVM](https://gist.github.com/stefanozanella/4608873)
* [Emulating Raspberry Pi on Linux](http://embedonix.com/articles/linux/emulating-raspberry-pi-on-linux/)
* [Emulating Raspberry Pi with QEMU](https://xbu.me/emulating-raspberry-pi-with-qemu/)
* [How to Emulate a Raspberry Pi on Your PC](http://www.makeuseof.com/tag/emulate-raspberry-pi-pc/)
* [Raspbian Stretch download](https://www.raspberrypi.org/downloads/raspbian/)



